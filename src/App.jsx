import React, { useState } from "react";
import './Style.css'
const App = () => {

  const [board, setBoard] = useState(Array(9).fill(" "));

  const [player, setPlayer] = useState('X');
  const [win, setWin] = useState(' ');


  function boxClicked(num) {


    console.log(num);


    board[num] = player;
    setBoard(board);

    if (player == 'X') {
      setPlayer('0');
    }
    else {

      setPlayer('X')

    }

    checkwinner();

  }


  function checkwinner() {

    const move = [

      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ]

    move.forEach((data) => {

      if (board[data[0]] == 'X' && board[data[1]] == 'X' && board[data[2]] == 'X') {

        console.log(setWin("winner X "));
        // window.location.reload();

      }
      if (board[data[0]] == '0' && board[data[1]] == '0' && board[data[2]] == '0') {

        console.log(setWin("winner 0"));
        // window.location.reload();
      }

    })



  }
  return (

    <>

      <h1>tic toe game</h1>
      <h1>{win}</h1>

      <table>

        <tr>
          <td onClick={() => boxClicked(0)}>{board[0]}</td>
          <td onClick={() => boxClicked(1)}>{board[1]}</td>
          <td onClick={() => boxClicked(2)}>{board[2]}</td>
        </tr>

        <tr>
          <td onClick={() => boxClicked(3)}>{board[3]}</td>
          <td onClick={() => boxClicked(4)}>{board[4]}</td>
          <td onClick={() => boxClicked(5)}>{board[5]}</td>
        </tr>
        <tr>
          <td onClick={() => boxClicked(6)}>{board[6]}</td>
          <td onClick={() => boxClicked(7)}>{board[7]}</td>
          <td onClick={() => boxClicked(8)}>{board[8]}</td>
        </tr>



      </table>

    </>

  )


}
export default App;
